# Task-manager

Альтернативный репозиторий
https://github.com/irb0000/jse

Это учебный проект в рамках курса по программе JAVA SPRING НЛМК.

## Требования к software

При разработке использовались:
* Java Openjdk version "11"
* Apache maven 3.6.1
* Windows 7 ultimate

## Описание стека технологий

Java SE + сборщик проектов Apache maven

## Разработчик

**Сычиков Владимир** vladimirsychikov@nospam.ru

## Сборка приложения 

```bash
mvn clean install -f pom.xml
```
## Запуск приложения

```bash
java -jar target/task-manager-1.0.0.jar
```
*Сборка и запуск из каталога проекта, в противном случае указывайте полный путь.*
## Поддерживаемые терминальные команды

```
help -вывод списка терминальных команд
version -информация о версии приложения
about -информация о разработчике
exit -выход из приложения

project-create -добавить проект
project-clear -удалить все проекты
project-list -вывести список проектов
project-view-by-index -просмотр проекта по имени
project-view-by-id -просмотр проекта по идентификатору
project-remove-by-id -удаление проекта по идентификатору
project-remove-by-name -удаление проекта по имени
project-remove-by-index -удаление проекта по индексу
project-update-by-index -обновление проекта по индексу
project-update-by-id -обновление проекта по идентификатору

task-create -добавить задачу
task-clear -удалить все задачи
task-list -вывести список задач
task-view-by-index -просмотр задачи по имени
task-view-by-id -просмотр задачи по идентификатору
task-remove-by-id -удаление задачи по идентификатору
task-remove-by-name -удаление задачи по имени
task-remove-by-index -удаление задачи по индексу
task-update-by-index -обновление задачи по индексу
task-update-by-id -обновление задачи по идентификатору
task-list-by-project-id -просмотр списка задач по идентификатору проекта
task-add-to-project-by-ids -добавление задачи в проект
task-remove-from-project-by-ids -удаление задачи из проекта

user-create -создание нового пользователя
user-clear -удаление всех пользователей
user-list -вывод списка всех пользователей
user-update-by-login -редактирование пользователя по его логину
user-update-by-ID -редактирование пользователя по его идентификатору
user-view-by-login -просмотр пользователя по его логину
user-view-by-id -просмотр пользователя по его идентификатору
user-remove-by-id -удаление пользователя по его идентификатору
user-remove-by-login -удаление пользователя по его логину
```