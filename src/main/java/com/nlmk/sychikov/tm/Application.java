package com.nlmk.sychikov.tm;

import com.nlmk.sychikov.tm.constant.RoleType;
import com.nlmk.sychikov.tm.controller.ProjectController;
import com.nlmk.sychikov.tm.controller.SystemController;
import com.nlmk.sychikov.tm.controller.TaskController;
import com.nlmk.sychikov.tm.controller.UserController;
import com.nlmk.sychikov.tm.entity.Task;
import com.nlmk.sychikov.tm.repository.ProjectRepository;
import com.nlmk.sychikov.tm.repository.TaskRepository;
import com.nlmk.sychikov.tm.repository.UserRepository;
import com.nlmk.sychikov.tm.service.ProjectService;
import com.nlmk.sychikov.tm.service.ProjectTaskService;
import com.nlmk.sychikov.tm.service.TaskService;
import com.nlmk.sychikov.tm.service.UserService;

import java.util.Scanner;

import static com.nlmk.sychikov.tm.constant.TerminalConst.*;

/**
 * Task manager
 *
 * @author Irb
 */
public class Application {

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final TaskRepository taskRepository = new TaskRepository();

    private final UserRepository userRepository = new UserRepository();

    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final TaskService taskService = new TaskService(taskRepository);

    private final UserService userService = new UserService(userRepository);

    private final UserController userController = new UserController(userService);

    private final ProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final TaskController taskController = new TaskController(taskService, projectTaskService);

    private final SystemController systemController = new SystemController();

    private final Scanner scanner = new Scanner(System.in);

    {
        projectRepository.create("Test project 1");
        projectRepository.create("Test project 2");
        final Task task = taskRepository.create("Task demo 1");
        task.setDescription("Description 1");
        taskRepository.create("Task demo 2");

        userService.addUser("irb0000", userController.encryptMd5("qwerty"),
                "qqqqq", "aaaaaa",
                "zzzzz", RoleType.ADMIN);
        userService.addUser("user0000", userController.encryptMd5("asdf"), "Иван", "Иванович", "Иванов");
    }

    /**
     * Main (start point)
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        final Application application = new Application();
        application.run(args);
        application.systemController.displayWelcome();
        application.process();
    }

    /**
     * Command line args processor
     *
     * @param args command line arguments
     */
    private void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    /**
     * One command processor
     *
     * @param param command
     * @return return value
     */
    private int run(final String param) {
        if (param == null || param.isEmpty()) return -1;
        switch (param) {
            case VERSION:
                return systemController.displayVersion();
            case ABOUT:
                return systemController.displayAbout();
            case HELP:
                return systemController.displayHelp();
            case EXIT:
                return systemController.exit();

            case PROJECT_CLEAR:
                return projectController.clearProject();
            case PROJECT_CREATE:
                return projectController.createProject();
            case PROJECT_LIST:
                return projectController.listProject();
            case PROJECT_VIEW_BY_INDEX:
                return projectController.viewProjectByIndex();
            case PROJECT_VIEW_BY_ID:
                return projectController.viewProjectById();
            case PROJECT_REMOVE_BY_ID:
                return projectController.removeProjectById();
            case PROJECT_REMOVE_BY_INDEX:
                return projectController.removeProjectByIndex();
            case PROJECT_REMOVE_BY_NAME:
                return projectController.removeProjectByName();
            case PROJECT_UPDATE_BY_INDEX:
                return projectController.updateProjectByIndex();
            case PROJECT_UPDATE_BY_ID:
                return projectController.updateProjectById();

            case TASK_CLEAR:
                return taskController.clearTask();
            case TASK_CREATE:
                return taskController.createTask();
            case TASK_LIST:
                return taskController.listTask();
            case TASK_VIEW_BY_INDEX:
                return taskController.viewTaskByIndex();
            case TASK_VIEW_BY_ID:
                return taskController.viewTaskById();
            case TASK_REMOVE_BY_ID:
                return taskController.removeTaskById();
            case TASK_REMOVE_BY_INDEX:
                return taskController.removeTaskByIndex();
            case TASK_REMOVE_BY_NAME:
                return taskController.removeTaskByName();
            case TASK_UPDATE_BY_INDEX:
                return taskController.updateTaskByIndex();
            case TASK_UPDATE_BY_ID:
                return taskController.updateTaskById();
            case TASK_LIST_BY_PROJECT_ID:
                return taskController.listTaskByProjectId();
            case TASK_REMOVE_FROM_PROJECT_BY_IDS:
                return taskController.removeTaskFromProjectByIds();
            case TASK_ADD_TO_PROJECT_BY_IDS:
                return taskController.addTaskToProjectByIds();

            case USER_CREATE:
                return userController.addUser();
            case USER_CLEAR:
                return userController.clearUsers();
            case USER_LIST:
                return userController.viewAllUsers();
            case USER_REMOVE_BY_ID:
                return userController.removeUserById();
            case USER_REMOVE_BY_LOGIN:
                return userController.removeUserByLogin();
            case USER_UPDATE_BY_ID:
                return userController.updateUserById();
            case USER_UPDATE_BY_LOGIN:
                return userController.updateUserByLogin();
            case USER_VIEW_BY_ID:
                return userController.viewUserById();
            case USER_VIEW_BY_LOGIN:
                return userController.viewUserByLogin();

            default:
                return systemController.displayError();
        }
    }

    /**
     * Infinite loop command processor with console input.
     * The "exit" command stops the loop.
     */
    private void process() {
        String command = "";
        while (!EXIT.equals(command)) {
            System.out.print("command:> ");
            command = scanner.nextLine();
            run(command);
            System.out.println();
        }
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public ProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    public TaskController getTaskController() {
        return taskController;
    }

    public ProjectController getProjectController() {
        return projectController;
    }

    public UserService getUserService() {
        return userService;
    }

    public UserController getUserController() {
        return userController;
    }
}
