package com.nlmk.sychikov.tm.controller;

import com.nlmk.sychikov.tm.entity.User;
import com.nlmk.sychikov.tm.service.UserService;

import java.util.List;

public class UserController extends AbstractController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Clear all users
     *
     * @return return value
     */
    public int clearUsers() {
        System.out.println("[CLEAR USERS]");
        userService.clear();
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Show the user
     *
     * @param user what user should be printed
     */
    public void viewUser(final User user) {
        if (user == null) return;
        System.out.println("[VIEW USER]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME :" + user.getFirstName());
        System.out.println("MIDDLE NAME :" + user.getMiddleName());
        System.out.println("LAST NAME :" + user.getLastName());
        System.out.println("[OK]");
    }

    /**
     * Shows user by index
     *
     * @return return value
     */
    public int viewUserByIndex() {
        final int index = intInputProcessor("Enter user index: ");
        if (index == -1)
            return 0;
        final User user = userService.findByIndex(index);
        if (user == null)
            System.out.println("User with index=" + (index + 1) + " is not found!");
        else viewUser(user);
        return 0;
    }

    /**
     * Shows user by id
     *
     * @return return value
     */
    public int viewUserById() {
        final Long id = longInputProcessor("Enter user id: ");
        if (id == -1)
            return 0;
        final User user = userService.findById(id);
        if (user == null)
            System.out.println("User with index=" + id.toString() + " is not found!");
        else viewUser(user);
        return 0;
    }

    /**
     * Shows user by login
     *
     * @return return value
     */
    public int viewUserByLogin() {
        System.out.print("Enter user login: ");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null)
            System.out.println("User with login=" + login + " is not found!");
        else viewUser(user);
        return 0;
    }

    /**
     * Removes user by login
     *
     * @return return value
     */
    public int removeUserByLogin() {
        System.out.println("[REMOVE USER]");
        System.out.print("Enter user login: ");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("User with login=" + login + " is not found!");
            System.out.println("[FAILED]");
            return -1;
        }
        userService.remove(user);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Removes user by id
     *
     * @return return value
     */
    public int removeUserById() {
        System.out.println("[REMOVE USER]");
        final Long id = longInputProcessor("Enter user id: ");
        if (id == -1)
            return -1;
        final User user = userService.findById(id);
        if (user == null) {
            System.out.println("User with id=" + id.toString() + " is not found!");
            System.out.println("[FAILED]");
            return -1;
        }
        userService.remove(user);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Updates user by "user" object
     */
    public void updateUser(final User user) {
        System.out.print("Enter first name[" + user.getFirstName() + "]: ");
        final String firstName = scanner.nextLine();
        System.out.print("Enter middle name[" + user.getMiddleName() + "]: ");
        final String middleName = scanner.nextLine();
        System.out.print("Enter last name[" + user.getLastName() + "]: ");
        final String lastName = scanner.nextLine();
        userService.update(user, firstName, middleName, lastName);
    }

    /**
     * Updates user by id
     *
     * @return return value
     */
    public int updateUserById() {
        System.out.println("[UPDATE USER]");
        final Long id = longInputProcessor("Enter user id: ");
        if (id == -1)
            return -1;
        final User user = userService.findById(id);
        if (user == null) {
            System.out.println("User with id=" + id.toString() + " is not found!");
            System.out.println("[FAILED]");
            return -1;
        }
        updateUser(user);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Updates user by login
     *
     * @return return value
     */
    public int updateUserByLogin() {
        System.out.println("[UPDATE USER]");
        System.out.print("Enter user login: ");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("User with login=" + login + " is not found!");
            System.out.println("[FAILED]");
            return -1;
        }
        updateUser(user);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Render user from list
     *
     * @param users user list
     */
    public void renderUsers(final List<User> users) {
        if (users == null || users.isEmpty()) return;
        int index = 1;
        for (User user : users) {
            System.out.println(index + ". " + user.toString());
            index++;
        }
    }

    /**
     * List all users
     *
     * @return return value
     */
    public int viewAllUsers() {
        System.out.println("[LIST USERS]");
        renderUsers(userService.findAll());
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Add new user
     *
     * @return return value
     */
    public int addUser() {
        System.out.println("[ADD NEW USER]");
        System.out.print("Enter new login: ");
        final String login = scanner.nextLine();
        if (userService.findByLogin(login) != null) {
            System.out.println("Login " + login + " already exists!");
            System.out.println("[FAILED]");
            return 0;
        }
        final String password = enterNewPassword();
        if (password == null || password.isEmpty()) {
            System.out.println("[FAILED]");
            return 0;
        }
        System.out.print("Enter first name[]: ");
        final String firstName = scanner.nextLine();
        System.out.print("Enter middle name[]: ");
        final String middleName = scanner.nextLine();
        System.out.print("Enter last name[]: ");
        final String lastName = scanner.nextLine();
        final User user = userService.addUser(login, encryptMd5(password), firstName, middleName, lastName);
        if (user == null) {
            System.out.println("[FAILED]");
            return 0;
        }
        System.out.println("[OK]");
        return 0;
    }

    /**
     * New password input dialog
     *
     * @return checked password or null
     */
    private String enterNewPassword() {
        System.out.print("Enter user password: ");
        final String password = scanner.nextLine();
        System.out.print("Confirm password: ");
        final String confirmedPassword = scanner.nextLine();
        final Boolean confirmed = checkPassword(password, confirmedPassword);
        if (!confirmed) {
            System.out.println("Passwords don't match or do not correspond the password agreement!");
            return null;
        }
        return password;
    }

    /**
     * Check passwords agreement and matching
     *
     * @param password          first password input
     * @param confirmedPassword second (repeated) password input
     * @return true if passwords correspond to the rule
     */
    private Boolean checkPassword(String password, String confirmedPassword) {
        return password.equals(confirmedPassword);
    }

}
