package com.nlmk.sychikov.tm.controller;

import java.util.Scanner;

public abstract class AbstractController {

    protected final Scanner scanner = new Scanner(System.in);

    /**
     * Int input processing with error message when not a number or empty value
     * or negative value received
     *
     * @return entered int value or -1 (when input is wrong)
     */
    protected int intInputProcessor(final String inputPrompt) {
        System.out.print(inputPrompt);
        final String stringValue = scanner.nextLine();
        try {
            final int i = Integer.parseInt(stringValue);
            if (i > 0)
                return i - 1;
            else
                System.out.println("WRONG INPUT VALUE '" + stringValue + "'! COMMAND TERMINATED.");
            return -1;
        } catch (NumberFormatException e) {
            System.out.println("WRONG INPUT VALUE '" + stringValue + "'! COMMAND TERMINATED.");
            return -1;
        }
    }

    /**
     * Long input processing with error message when not a number or empty value
     * or negative value received
     *
     * @return entered Long value or -1 (when input is wrong)
     */
    protected Long longInputProcessor(final String inputPrompt) {
        System.out.print(inputPrompt);
        final String stringValue = scanner.nextLine();
        try {
            final long l = Long.parseLong(stringValue);
            if (l > 0L)
                return l;
            else
                System.out.println("WRONG INPUT VALUE '" + stringValue + "'! COMMAND TERMINATED.");
            return (long) -1;
        } catch (NumberFormatException e) {
            System.out.println("WRONG INPUT VALUE '" + stringValue + "'! COMMAND TERMINATED.");
            return (long) -1;
        }
    }

    /**
     * MD5 string encryption
     *
     * @param md5 input string
     * @return md5 coded string
     */
    public String encryptMd5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

}
