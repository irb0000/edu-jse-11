package com.nlmk.sychikov.tm.controller;

import static com.nlmk.sychikov.tm.constant.TerminalConst.*;

public class SystemController {

    /**
     * Show welcome string
     */
    public void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    /**
     * Show available command
     *
     * @return return value
     */
    public int displayHelp() {
        System.out.println(VERSION + " - display the version info");
        System.out.println(HELP + " - display the list of terminal commands");
        System.out.println(ABOUT + " - display the developer info");
        System.out.println(EXIT + " - close the program");
        System.out.println();
        System.out.println(PROJECT_CREATE + " - create the new project");
        System.out.println(PROJECT_CLEAR + " - clear all projects");
        System.out.println(PROJECT_LIST + " - list all projects");
        System.out.println(PROJECT_VIEW_BY_INDEX + " - shows project by  index");
        System.out.println(PROJECT_VIEW_BY_ID + " - shows project by id");
        System.out.println(PROJECT_UPDATE_BY_INDEX + " - updates project by index");
        System.out.println(PROJECT_UPDATE_BY_ID + " - updates project by id");
        System.out.println(PROJECT_REMOVE_BY_INDEX + " - removes project by index");
        System.out.println(PROJECT_REMOVE_BY_ID + " - removes project by id");
        System.out.println(PROJECT_REMOVE_BY_NAME + " - removes project by name");
        System.out.println();
        System.out.println(TASK_CREATE + " - create the new task");
        System.out.println(TASK_CLEAR + " - clear all tasks");
        System.out.println(TASK_LIST + " - list all tasks");
        System.out.println(TASK_VIEW_BY_INDEX + " - shows task by index");
        System.out.println(TASK_VIEW_BY_ID + " - shows task by id");
        System.out.println(TASK_UPDATE_BY_INDEX + " - updates task index");
        System.out.println(TASK_UPDATE_BY_ID + " - updates task by id");
        System.out.println(TASK_REMOVE_BY_INDEX + " - removes task by index");
        System.out.println(TASK_REMOVE_BY_ID + " - removes task by id");
        System.out.println(TASK_REMOVE_BY_NAME + " - removes task by name");
        System.out.println(TASK_LIST_BY_PROJECT_ID + " - show task list by project id");
        System.out.println(TASK_ADD_TO_PROJECT_BY_IDS + " - add task to project by ids");
        System.out.println(TASK_REMOVE_FROM_PROJECT_BY_IDS + " - remove task from project by ids");
        System.out.println(USER_CREATE + " - create new user");
        System.out.println(USER_CLEAR + " - clear all users");
        System.out.println(USER_LIST + " - view all users list");
        System.out.println(USER_UPDATE_BY_LOGIN + " - update user by login");
        System.out.println(USER_UPDATE_BY_ID + " - update user by ID");
        System.out.println(USER_VIEW_BY_LOGIN + " - view user by login");
        System.out.println(USER_VIEW_BY_ID + " - view user by ID");
        System.out.println(USER_REMOVE_BY_ID + " - remove user by id");
        System.out.println(USER_REMOVE_BY_LOGIN + " - remove user by login");

        return 0;
    }

    /**
     * Show error message
     *
     * @return return value
     */
    public int displayError() {
        System.out.println("Error! Unknown command...");
        return -1;
    }

    /**
     * Show version
     *
     * @return return value
     */
    public int displayVersion() {
        System.out.println("1.0.0");
        return 0;
    }

    /**
     * Show exit string
     *
     * @return value
     */
    public int exit() {
        System.out.println("Our program exit now...");
        System.out.println("Bye!");
        return 0;
    }

    /**
     * Show info
     *
     * @return return value
     */
    public int displayAbout() {
        System.out.println("Vladimir Sychikov");
        System.out.println("VladimirSychikov@nospam.ru");
        return 0;
    }

}
