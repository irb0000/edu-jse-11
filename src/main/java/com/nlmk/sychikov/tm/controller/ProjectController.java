package com.nlmk.sychikov.tm.controller;

import com.nlmk.sychikov.tm.entity.Project;
import com.nlmk.sychikov.tm.service.ProjectService;
import com.nlmk.sychikov.tm.service.ProjectTaskService;

public class ProjectController extends AbstractController {

    private final ProjectService projectService;

    private final ProjectTaskService projectTaskService;

    public ProjectController(ProjectService projectService, ProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    /**
     * Add new project
     *
     * @return return value
     */
    public int createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.print("Enter project name: ");
        final String name = scanner.nextLine();
        System.out.print("Enter project description: ");
        final String description = scanner.nextLine();
        if (projectService.create(name, description) == null) {
            System.out.println("[FAILED]");
        }
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Update project by id
     *
     * @return return value
     */
    public int updateProjectById() {
        System.out.println("[UPDATE PROJECT BY ID]");
        final Long id = longInputProcessor("Enter project id: ");
        if (id == -1)
            return 0;
        final Project project = projectService.findById(id);
        if (project == null) {
            System.out.println("[Project id='" + id.toString() + "' is not found!]");
            return 0;
        }
        System.out.print("Enter project name: ");
        final String name = scanner.nextLine();
        System.out.print("Enter project description: ");
        final String description = scanner.nextLine();
        projectService.update(id, name, description);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Update project by index
     *
     * @return return value
     */
    public int updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT BY ID]");
        final int index = intInputProcessor("Enter project id: ");
        if (index == -1)
            return 0;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("[Project with index='" + (index + 1) + "' is not found!]");
            return 0;
        }
        System.out.print("Enter project name: ");
        final String name = scanner.nextLine();
        System.out.print("Enter project description: ");
        final String description = scanner.nextLine();
        projectService.update(project.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Removes project by name
     *
     * @return return value
     */
    public int removeProjectByName() {
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.print("Enter project name: ");
        final String name = scanner.nextLine();
        final Project project = projectTaskService.removeProjectByName(name);
        if (project == null) System.out.println("[Project '" + name + "' is not found!]");
        else System.out.println("[OK]");
        return 0;
    }

    /**
     * Removes project by id
     *
     * @return return value
     */
    public int removeProjectById() {
        System.out.println("[REMOVE PROJECT BY ID]");
        final Long id = longInputProcessor("Enter project id: ");
        if (id == -1)
            return 0;
        final Project project = projectTaskService.removeProjectById(id);
        if (project == null) System.out.println("[Project with id=" + id.toString() + " is not found!]");
        else System.out.println("[OK]");
        return 0;
    }

    /**
     * Removes project by index
     *
     * @return return value
     */
    public int removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        final int index = intInputProcessor("Enter project index: ");
        if (index == -1)
            return 0;
        final Project project = projectTaskService.removeProjectByIndex(index);
        if (project == null)
            System.out.println("[Project with index=" + (index + 1) + " is not found!]");
        else System.out.println("[OK]");
        return 0;
    }

    /**
     * Clear all projects
     *
     * @return return value
     */
    public int clearProject() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        System.out.println("[OK]");
        return 0;
    }

    /**
     * List all projects
     *
     * @return return value
     */
    public int listProject() {
        System.out.println("[LIST PROJECTS]");
        int index = 1;
        for (Project project : projectService.findAll()) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName() + ": " + project.getDescription());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Show the project
     *
     * @param project what project should be printed
     */
    public void viewProject(final Project project) {
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID:" + project.getId());
        System.out.println("NAME:" + project.getName());
        System.out.println("DESCRIPTION:" + project.getDescription());
        System.out.println("[OK]");
    }

    /**
     * Shows project by index
     *
     * @return return value
     */
    public int viewProjectByIndex() {
        final int index = intInputProcessor("Enter project index: ");
        if (index == -1)
            return 0;
        final Project project = projectService.findByIndex(index);
        viewProject(project);
        return 0;
    }

    /**
     * Shows project by id
     *
     * @return return value
     */
    public int viewProjectById() {
        final Long id = longInputProcessor("Enter project id: ");
        if (id == -1)
            return 0;
        final Project project = projectService.findById(id);
        if (project == null) {
            System.out.println("[Project id='" + id.toString() + "' is not found!]");
            return 0;
        }
        viewProject(project);
        return 0;
    }

}
