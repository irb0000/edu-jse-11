package com.nlmk.sychikov.tm.service;

import com.nlmk.sychikov.tm.constant.RoleType;
import com.nlmk.sychikov.tm.entity.User;
import com.nlmk.sychikov.tm.repository.UserRepository;

import java.util.List;

public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User addUser(final String login, final String passwordHash, final String firstName, final String middleName,
                        final String lastName, RoleType roleType) {
        if (login == null || login.isEmpty()) return null;
        if (passwordHash == null || passwordHash.isEmpty()) return null;
        if (firstName == null || firstName.isEmpty())
            return userRepository.addUser(login, passwordHash);
        if (middleName == null || middleName.isEmpty()) return null;
        if (lastName == null || lastName.isEmpty()) return null;
        if (userRepository.findByLogin(login) != null) return null;
        return userRepository.addUser(login, passwordHash, firstName, middleName, lastName, roleType);
    }

    public User addUser(final String login, final String passwordHash, final String firstName, final String middleName,
                        final String lastName) {
        if (login == null || login.isEmpty()) return null;
        if (passwordHash == null || passwordHash.isEmpty()) return null;
        if (firstName == null || firstName.isEmpty())
            return userRepository.addUser(login, passwordHash);
        if (middleName == null || middleName.isEmpty()) return null;
        if (lastName == null || lastName.isEmpty()) return null;
        if (userRepository.findByLogin(login) != null) return null;
        return userRepository.addUser(login, passwordHash, firstName, middleName, lastName);
    }

    public User addUser(final String login, final String passwordHash) {
        if (login == null || login.isEmpty()) return null;
        if (passwordHash == null || passwordHash.isEmpty()) return null;
        return userRepository.addUser(login, passwordHash);
    }

    public void clear() {
        userRepository.clear();
    }

    public User findByIndex(final int index) {
        if (index > userRepository.getRepositorySize() - 1 || index < 0) return null;
        return userRepository.findByIndex(index);
    }

    public User findById(final Long id) {
        if (id == null) return null;
        return userRepository.findById(id);
    }

    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.findByLogin(login);
    }

    public User remove(final User user) {
        if (user == null) return null;
        return userRepository.remove(user);
    }

    public User update(final User user, final String firstName, final String middleName,
                       final String lastName) {
        if (user == null) return null;
        return userRepository.update(user, firstName, middleName, lastName);
    }

    public User removeByIndex(final int index) {
        final User user = findByIndex(index);
        return remove(user);
    }

    public User removeById(final Long id) {
        final User user = findById(id);
        return remove(user);
    }

    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        return remove(user);
    }

    private User setPasswordHash(final User user, final String passwordHash) {
        if (user == null) return null;
        if (passwordHash == null) return null;
        user.setPasswordHash(passwordHash);
        return user;
    }

    public User setPasswordByLogin(final String login, final String passwordHash) {
        if (login == null || login.isEmpty()) return null;
        if (passwordHash == null || passwordHash.isEmpty()) return null;
        final User user = findByLogin(login);
        return setPasswordHash(user, passwordHash);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

}
