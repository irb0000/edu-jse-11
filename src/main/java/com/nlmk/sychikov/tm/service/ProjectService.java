package com.nlmk.sychikov.tm.service;

import com.nlmk.sychikov.tm.entity.Project;
import com.nlmk.sychikov.tm.repository.ProjectRepository;

import java.util.List;

public class ProjectService {

    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Project create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name);
    }

    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.create(name, description);
    }

    public Project update(final Long id, final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        if (id == null) return null;
        return projectRepository.update(id, name, description);
    }

    public void clear() {
        projectRepository.clear();
    }

    public Project findByIndex(final int index) {
        if (index > projectRepository.getRepositorySize() - 1 || index < 0) return null;
        return projectRepository.findByIndex(index);
    }

    public Project findById(final Long id) {
        if (id == null) return null;
        return projectRepository.findById(id);
    }

    /*Following methods is not used now. See ProjectTaskService for reference*/
    public Project findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByName(name);
    }

    public Project removeById(final Long id) {
        if (id == null) return null;
        return projectRepository.removeById(id);
    }

    public Project removeByIndex(final int index) {
        if (index > projectRepository.getRepositorySize() - 1 || index < 0) return null;
        return projectRepository.removeByIndex(index);
    }

    public Project removeByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeByName(name);
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

}
