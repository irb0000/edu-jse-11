package com.nlmk.sychikov.tm.repository;

import com.nlmk.sychikov.tm.constant.RoleType;
import com.nlmk.sychikov.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

/**
 * User storage
 */
public class UserRepository {

    private final List<User> users = new ArrayList<>();

    public User addUser(final String login, final String passwordHash, final String firstName, final String middleName,
                        final String lastName, final RoleType roleType) {
        final User user = new User(login, passwordHash, firstName, middleName, lastName);
        user.setRoleType(roleType);
        users.add(user);
        return user;
    }

    public User addUser(final String login, final String passwordHash, final String firstName, final String middleName,
                        final String lastName) {
        final User user = new User(login, passwordHash, firstName, middleName, lastName);
        users.add(user);
        return user;
    }

    public User addUser(final String login, final String passwordHash) {
        User user = new User(login, passwordHash);
        users.add(user);
        return user;
    }

    public int getRepositorySize() {
        return users.size();
    }

    public User update(final User user, final String firstName, final String middleName,
                       final String lastName) {
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        return user;
    }

    /*TODO delete after refactor (later)

        public User updateByIndex(final int index, final String firstName, final String middleName,
                                  final String lastName) {
            final User user = findByIndex(index);
            if (user == null) return null;
            user.setFirstName(firstName);
            user.setMiddleName(middleName);
            user.setLastName(lastName);
            return user;
        }

        public User updateByLogin(final String login, final String firstName, final String middleName,
                                  final String lastName) {
            final User user = findByLogin(login);
            if (user == null) return null;
            user.setFirstName(firstName);
            user.setMiddleName(middleName);
            user.setLastName(lastName);
            return user;
        } */

    public User findById(final Long id) {
        for (final User user : users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    public User findByIndex(final int index) {
        return users.get(index);
    }

    public User findByLogin(final String login) {
        for (final User user : users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    public User remove(final User user) {
        users.remove(user);
        return user;
    }

    public void clear() {
        users.clear();
    }

    public List<User> findAll() {
        return users;
    }

    public User setPasswordHash(final User user, String passwordHash) {
        user.setPasswordHash(passwordHash);
        return user;
    }

}
