package com.nlmk.sychikov.tm.repository;

import com.nlmk.sychikov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

/**
 * Tasks storage
 */
public class TaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    public Task create(final String name) {
        final Task task = new Task(name);
        tasks.add(task);
        return task;
    }

    public Task create(final String name, final String description) {
        final Task task = new Task(name, description);
        tasks.add(task);
        return task;
    }

    public int getRepositorySize() {
        return tasks.size();
    }

    public Task update(final Long id, final String name, final String description) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    public void clear() {
        tasks.clear();
    }

    public Task findByIndex(final int index) {
        return tasks.get(index);
    }

    public Task findById(final Long id) {
        for (final Task task : tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    public Task findByName(final String name) {
        for (final Task task : tasks) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    public Task removeById(final Long id) {
        final Task task = findById(id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    public Task removeByIndex(final int index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    public Task findByProjectIdAndId(final Long projectId, final Long id) {
        final Task task = findById(id);
        if (task == null) return null;
        final Long idProject = task.getProjectId();
        if (idProject == null) return null;
        if (idProject.equals(projectId)) return task;
        return null;
    }

    public List<Task> findAll() {
        return tasks;
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : tasks) {
            Long Idproject = task.getProjectId();
            if (Idproject == null) continue;
            if (projectId.equals(Idproject)) result.add(task);
        }
        return result;
    }

    public Task removeFromProjectByIds(final Long projectId, final Long id) {
        final Task task = findByProjectIdAndId(projectId, id);
        if (task == null) return null;
        task.setProjectId(null);
        return task;

    }

}
